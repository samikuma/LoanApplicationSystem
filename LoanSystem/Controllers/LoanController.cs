﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoanSystem.Models;

namespace LoanSystem.Controllers
{
    public class LoanController : Controller
    {
        // GET: Loan
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateNewApplicationForm()
        {
            return PartialView("_CreateLoan");
        }
    }
}