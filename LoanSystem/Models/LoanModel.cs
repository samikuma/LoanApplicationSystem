﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoanSystem.Models
{
    public class LoanModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address { get; set; }
        public string income { get; set; }
        public string loanAmount { get; set; }
        public string creditStatus { get; set; }       
    }
}